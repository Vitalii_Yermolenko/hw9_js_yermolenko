
 const arr1 = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
 const arr2 = ["1", "2", "3", "sea", "user", 23];
 const arr3 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
 const paragr = document.createElement('p');
 document.body.append(paragr);
 let i = 3;
 paragr.innerText = 'Час до очистки: ' + i + 'секунд';
setInterval(() => {
   i--;
   paragr.innerText = 'Час до очистки: ' + i + 'секунд';
},1000)

 function removeAll() {
   
   document.body.innerText= '';
}

function getList(arr,parent){
   const ul = document.createElement('ul');
   parent.append(ul);
   const new_array = arr.map((item) => {
      const li = document.createElement('li');
      ul.append(li);
      if(typeof(item) === 'object'){
         getList(item,li);
      }else{
         li.innerText = item;
      }
   } )

}

getList(arr1,document.body);
getList(arr2,document.body);
getList(arr3,document.body);

setTimeout(removeAll, 3000);